# Avenue Code UI Challenge - Part II #

### Story Phrase ###
* As a curious and sometimes forgetful web surfer, I want to add a simple history page to my GeoLocation web application, so that I can see all the URLs searched by me on the current session.
* I sometimes use a desktop, sometimes a tablet and sometimes a cell phone, so I need a responsive web page.

### Business Narrative / Scenario ###
* You need to augment Geolocation by providing the history of all the domains typed in by the user, with the most critical info (URL, IP, Country, Latitude, Longitude and Date).

### Functional / Acceptance Criteria ###
* You can choose any approach for the history panel, such as a new page, an overlay, a sidebar...
* Each domain in this history should be a link, which when clicked will ultimately display its location again on the map along with its details.

### Non-Functional / Acceptance Criteria ###
* You can keep track of the information the way you think is best: JS framework, JS array, LocalStorage/SessionStorage, NoSQL database, backend...
* You have to cover all the new code with JS testing. You can pick a framework of your preference like Jasmine, Chai, Mocha, QUnit.
* If you don't have test coverage on all the code delivered on Part I, you MUST cover that code as well now.
* Don't break existing functionality.
* Feel free to add any extra library to your app.

### Delivery Instructions ###
1. Fork/branch your code on BitBucket.
1. You will be personally evaluated as soon as you finish the test.
1. After you are done, you must push all your code and give the user **ac-recruitment** read permission on your repository. That is just for any additional evaluation the recruiter might want to do.

### Evaluation Criteria ###
1. Functional and non-functional acceptance criteria delivery
1. Code quality: structure, modularization, reuse
1. Code legibility and elegancy
1. Frameworks usage
1. Test coverage
1. Number of bugs
