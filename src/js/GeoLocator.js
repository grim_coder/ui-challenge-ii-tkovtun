var React = require('react');
var Gmaps = require('react-gmaps').Gmaps
var InfoWindow = require('react-gmaps').InfoWindow
var $ = require('jquery')

const coords = {
    lat: 31.5258541,
    lng: -0.08040660000006028
};

const regex =/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/

var result;

module.exports = React.createClass({


    getInitialState(){
        return {domain: undefined, myLocation: undefined, domainDisabled: 'disabled', isHistoryVisible : false, history: {} }
    },

    domainChanged(){
        var domainname = this.refs.domain.value
        var domainDisabled = 'disabled'
        if (result = regex.exec(domainname)) {

            domainDisabled = ''
        }

        this.setState({domainDisabled: domainDisabled})

    },
    locate: function (e, key) {

        var _this = this

        var domainname = this.refs.domain.value
        if (e){
            domainname = e
        }
        if (result = regex.exec(domainname)) {
            $.ajax({
                type: 'GET',
                url: 'http://ip-api.com/json/' + domainname,
                success: function (response) {
                    response.domainname = domainname

                    var _history = _this.state.history;


                    _history[domainname] = response
                    _history[domainname].domain = domainname


                    _this.setState({domain: response, history: _history})
                }
            })
        }
    },

    locateMe(){
        var _this = this
        $.ajax({
            type: 'GET',
            url: 'http://ip-api.com/json/',
            success: function (response) {
                _this.setState({myLocation: response})
            }
        });
    },

    reset(){
        this.setState({myLocation: undefined})
    },

    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    },

    onDragEnd(e) {
        console.log('onDragEnd', e);
    },

    onCloseClick() {
        console.log('onCloseClick');
    },

    onClick(e) {
        console.log('onClick', e);
    },

    ShowHistory() {
        this.setState({isHistoryVisible: !this.state.isHistoryVisible})

    },

    historyClicked(key){
        this.locate(key)

    },

    render() {

        var historyDiv = '';
        var links = []
        if (this.state.isHistoryVisible) {

            Object.keys(this.state.history).map((key)=>{
                links.push(<li key={key}>
                    <a onClick={this.historyClicked.bind(null, key)} >{this.state.history[key].domain}</a>
                </li>)
            })



            historyDiv = <div className="historyDiv">
                <h5>History</h5>
                <ul>
                    {links}
                </ul>
            </div>
        }



        var markers = []

        if (this.state.domain) {

            var content =
                '<div><b>' + this.state.domain.domainname +'</b></div>'+
                '<table align="center"  className="domainLocation" class="empty">'+
                '<tbody>'+
                '<tr>'+
                '<td class="field_name">IP</td>'+
                '<td id="location_query" class="location_value">' + this.state.domain.query+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Country</td>'+
                '<td id="location_country" class="location_value">' + this.state.domain.country+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Region</td>'+
                '<td id="location_regionName" class="location_value">'+this.state.domain.regionName+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">City</td>'+
                '<td id="location_city" class="location_value">'+this.state.domain.city+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Time Zone</td>'+
                '<td id="location_timezone" class="location_value">'+this.state.domain.timezone+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Latitude</td>'+
                '<td id="location_lat" class="location_value">'+this.state.domain.lat+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Longitude</td>'+
                '<td id="location_lon" class="location_value">'+this.state.domain.lon+'</td>'+
                '</tr>'+
                '</tbody>'+
                '</table>'

            markers.push(<InfoWindow key="domain" class="domainInfoWindow"
                                     title='domain' ref="domainInfoWindow" content={content}
                                     lat={this.state.domain.lat}
                                     lng={this.state.domain.lon}
            />)
        }

        if (this.state.myLocation) {

            var content =
                '<div className="myLocationRef" ref="myLocationRef"><b>My location</b></div>'+
                '<table className="myLocation" align="center" class="empty">'+
                '<tbody>'+
                '<tr>'+
                '<td class="field_name">IP</td>'+
                '<td id="location_query" ref="myLocationQuery" class="location_value">' + this.state.myLocation.query+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Country</td>'+
                '<td id="location_country" class="location_value">' + this.state.myLocation.country+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Region</td>'+
                '<td id="location_regionName" class="location_value">'+this.state.myLocation.regionName+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">City</td>'+
                '<td id="location_city" class="location_value">'+this.state.myLocation.city+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Time Zone</td>'+
                '<td id="location_timezone" class="location_value">'+this.state.myLocation.timezone+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Latitude</td>'+
                '<td id="location_lat" class="location_value">'+this.state.myLocation.lat+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td class="field_name">Longitude</td>'+
                '<td id="location_lon" class="location_value">'+this.state.myLocation.lon+'</td>'+
                '</tr>'+
                '</tbody>'+
                '</table>'


            markers.push(<InfoWindow key="myLocation" title='Home' label='Home' content={content}
                                     lat={this.state.myLocation.lat}
                                     lng={this.state.myLocation.lon}



            />)
        }

        return (
            <div ref="main"  >
                <Gmaps className="map"
                       lat={coords.lat}
                       lng={coords.lng}
                       zoom={2}
                       params={{v: '3.exp'}}
                       onMapCreated={this.onMapCreated}>
                    {markers}
                </Gmaps>

            <span  ref="controls"  className="controls">

                <input onChange={this.domainChanged} ref="domain" autofocus="autofocus" className="search bu" type="text" placeholder="website"/>
                    <button onClick={this.locate.bind(null, null)} ref='locateButton' className={"btn bg-primary btn-responsive bu " + this.state.domainDisabled}>Locate</button>
                    <button onClick={this.locateMe} ref='myLocationButton' className="btn btn-responsive bg-info bu">My location</button>
                    <button onClick={this.reset} ref='resetButton' className="btn btn-primary btn-responsive bu ">Reset</button>
                    <button onClick={this.ShowHistory} ref='showHistory' className="btn btn-primary btn-responsive ">History</button>

            </span>
                {historyDiv}
            </div>



        )
    }
})

