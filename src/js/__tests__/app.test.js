/**
 * Created by taraskovtun on 2/18/16.
 */
jest.dontMock('../GeoLocator');
var $ = require('jquery')
const ReactDOM = require('react-dom')
const TestUtils = require('react-addons-test-utils')
var React = require('react');
var GeoLocator = require('../GeoLocator');


describe('GeoLocator', function () {

    var locator
    var component = React.createFactory(GeoLocator)();

    beforeEach(function () {
        locator = TestUtils.renderIntoDocument(component)
    })

    it('Render properly', function () {

        expect(TestUtils.isCompositeComponent(locator)).toBeTruthy();
        var buttons = TestUtils.scryRenderedDOMComponentsWithClass(locator, 'btn');
        var map = TestUtils.scryRenderedDOMComponentsWithClass(locator, 'map');

        expect(buttons.length).toEqual(4);
        expect(map.length).toEqual(1);
    })


    it('updated my location state when My Location button is clicked', function () {

        var myLocationButton = locator.refs.myLocationButton
        TestUtils.Simulate.click(myLocationButton)
        waitsFor(()=> {
            return locator.state.myLocation
        }, 'Ajax call to http://ip-api.com/json/domainname', 5000)

        runs(function () {
            expect(locator.state.myLocation).toBeTruthy()
        });
    })

    it('When reset button is clicked my location should be reset to undefined', function () {

        var myLocationButton = locator.refs.myLocationButton

        TestUtils.Simulate.click(myLocationButton)
        waitsFor(()=> {
            return locator.state.myLocation
        }, 'Ajax call to http://ip-api.com/json/domainname', 5000)

        runs(function () {
            expect(locator.state.myLocation).toBeTruthy()
            var resetButton = locator.refs.resetButton
            TestUtils.Simulate.click(resetButton)
            expect(locator.state.myLocation).toBeFalsy()
        });


    })

    it('when entered incorrect domain locate must be disabled', function () {

        var domainInput = locator.refs.domain
        domainInput.value = 'sss'
        TestUtils.Simulate.change(domainInput);
        expect(locator.state.domainDisabled).toBe('disabled');
        var locateButton = locator.refs.locateButton
        expect(locateButton.className.indexOf('disabled')).toBeGreaterThan(-1)

    })

    it('when entered correct domain locate must be enabled', function () {

        var domainInput = locator.refs.domain
        domainInput.value = 'www.google.com'
        TestUtils.Simulate.change(domainInput);
        expect(locator.state.domainDisabled).toBe('');
        var locateButton = locator.refs.locateButton
        expect(locateButton.className.indexOf('disabled')).toBe(-1)
    })

    it('Updated location state when Locate button is clicked.', function () {

        var domainInput = locator.refs.domain
        domainInput.value = 'www.mail.ru'
        var locateButton = locator.refs.locateButton
        TestUtils.Simulate.click(locateButton)

        waitsFor(()=> {
            return locator.state.domain
        }, 'Ajax call to http://ip-api.com/json/domainname', 5000)

        runs(function () {
            console.log(locator.state.domain)
            expect(locator.state.domain).toBeTruthy()

            expect(locator.state.history).toBeTruthy()
            expect(locator.state.history['www.mail.ru']).toBeTruthy()


        });
    })

    it('on click history is shown another click - hide', function () {

        expect(locator.state.isHistoryVisible).toBeFalsy()

        var showHistoryButton = locator.refs.showHistory
        TestUtils.Simulate.click(showHistoryButton)
        expect(locator.state.isHistoryVisible).toBeTruthy()
        TestUtils.Simulate.click(showHistoryButton)
        expect(locator.state.isHistoryVisible).toBeFalsy()

    })


})