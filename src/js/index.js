/**
 * Created by taraskovtun on 2/18/16.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var GeoLocator = require('./GeoLocator')

ReactDOM.render(<GeoLocator />, document.getElementById('app'));
